import Dashboard from "./components/dashboard";
import LoginForm from "./components/loginForm";
import { UseAuthContext } from "./context/authContext";
import TodoProvider from "./context/todoContext";

function App() {
    const { isLoggedIn } = UseAuthContext();

    return (
        <>
        {isLoggedIn ? (
            <TodoProvider>
                <div className="flex justify-center items-center h-screen">
                    <Dashboard />
                </div>
            </TodoProvider>
        ) : (
            <LoginForm />
        )}
        </>
    );
}

export default App;
