import { useContext, createContext, useState, useEffect } from "react";

interface IAuthContext {
    isLoggedIn: boolean;
    logIn(): void;
    logOut(): void;
}

export const AuthContext = createContext<IAuthContext>({ 
    isLoggedIn: false,
    logIn: () => {},
    logOut: () => {}
});

const AuthProvider = ({ children}: any) => {
    const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);

    useEffect(() => {
        const data = localStorage.getItem("isLoggedIn");
        if (data !== null) {
            setIsLoggedIn(JSON.parse(data));
        }
    }, []);

    const logInHandler = () => {
        setIsLoggedIn(true);
        localStorage.setItem("isLoggedIn", JSON.stringify(true));
    };
    
    const logOuthandler = () => {
        setIsLoggedIn(false);
        localStorage.setItem("isLoggedIn", JSON.stringify(false));
    };

    return (
        <AuthContext.Provider value={{ isLoggedIn, logIn: logInHandler, logOut: logOuthandler }}>
            {children}
        </AuthContext.Provider>
    )
}

export const UseAuthContext = () => {
    return useContext(AuthContext)
}

export default AuthProvider;