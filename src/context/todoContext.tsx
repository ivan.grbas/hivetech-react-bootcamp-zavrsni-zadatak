import { createContext, useState, useEffect, useContext } from "react";
import { db } from "../utils/firebase";
import { doc, addDoc, getDocs, deleteDoc, collection, updateDoc } from "firebase/firestore";
import { ITodo } from "../models/ITodo";

interface ITodoContext {
    todos: ITodo[];
    addTodo(text: string): void;
    removeTodo(id: string): void;
    setCompleted(id: string, completed: boolean): void
}

export const TodoContext = createContext<ITodoContext>({
    todos: [],
    addTodo: () => {},
    removeTodo: () => {},
    setCompleted: () => {}
});

const TodoProvider = ({ children }: any) => {
    const [todos, setTodos] = useState<ITodo[]>([]);

    useEffect(() => {
        getDocs(collection(db, "todos")).then((querySnapshot) => {
            const data = querySnapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    text: doc.data().text,
                    completed: doc.data().completed
                }
            });

            setTodos(data);
        }); 
    }, []);

    const addTodo = async (text: string) => {
        const response = await addDoc(collection(db, "todos"), {
            text: text,
            completed: false
        });

        setTodos([...todos, { id: response.id, text: text, completed: false }]);
    }

    const removeTodo = async (id: string) => {
        await deleteDoc(doc(db, "todos", id));

        setTodos(todos.filter((todo) => todo.id !== id));
    }

    const setCompleted = async (id: string, completed: boolean) => {
        await updateDoc(doc(db, "todos", id), { completed: completed });

        const newTodos = todos.map((todo) => {
            if (todo.id === id) {
                todo.completed = completed;
            }

            return todo;
        });

        setTodos(newTodos);
    }

    return (
        <TodoContext.Provider value={{ todos, addTodo, removeTodo, setCompleted }}>
            {children}
        </TodoContext.Provider>
    )
}

export const UseTodoContext = () => {
    return useContext(TodoContext)
}

export default TodoProvider;