import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';

const app = initializeApp({
    apiKey: "AIzaSyAjgEkn1-MTukN_uvluVjJqtyacaRaeP2k",
    authDomain: "todo-app-60c03.firebaseapp.com",
    projectId: "todo-app-60c03",
    storageBucket: "todo-app-60c03.appspot.com",
    messagingSenderId: "111974203237",
    appId: "1:111974203237:web:ce4e14aa4aa8ae9bb86b8c",
    measurementId: "G-THX3FV9D53"
});

const db = getFirestore(app);

export { db }