import Modal from 'react-modal';
import { useState } from 'react';
import { UseTodoContext } from '../context/todoContext';

export function AddTodoModal({ isOpen, setIsOpen }: any) {
    const { addTodo } = UseTodoContext();
    const [text, setText] = useState<string>("");
    const [isValid, setIsValid] = useState<boolean>(true);

    const submit = (e: any) => {
        e.preventDefault();

        if (text.length === 0) {
            setIsValid(false)
            return;
        }
        
        addTodo(text);
        closeModal();
    }

    const closeModal = () => {
        setText("");
        setIsOpen(false);
    }

    const onInputChange = (e: any) => {
        const value = e.target.value;
        setText(value);
        setIsValid(true);
    }

    return (
        <Modal 
            className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 border border-gray-500 bg-white rounded-md w-full max-w-4xl"
            isOpen={isOpen} 
            appElement={document.getElementById('root') as HTMLElement} 
        >
            <button className="absolute top-0 right-2 p-1" onClick={closeModal}>x</button>
            <div className="flex flex-col items-center px-5">
                <span className="w-full text-center mb-4 mt-4 font-serif">Add new todo</span>
                <form onSubmit={submit} className="flex flex-col w-full items-center">
                    <input type="text" onChange={onInputChange} className={`border px-2 rounded-md h-8 ${isValid ? 'border-gray-300' : 'border-red-500'} mb-7 w-full`}/>
                    <input type="submit" className="mb-4 rounded-md py-1 bg-green-400 text-white w-1/6 hover:cursor-pointer" value="Save"/>
                </form>
            </div>
        </Modal>
    )
}