import { ITodo } from '../models/ITodo';
import { UseTodoContext } from '../context/todoContext';

export default function Todo(props: ITodo) {
    const { removeTodo, setCompleted } = UseTodoContext();

    const toggleCompleted = () => {
        setCompleted(props.id, !props.completed);
    }

    return (
        <div className={`flex justify-between w-full shadow-md rounded-xl ${props.completed ? 'bg-green-500 text-white' : 'bg-white'} p-3 mb-3`}>
            <span className="pr-4">{props.text}</span>
            <div className="flex items-center">
                <input className="w-5 h-5 mr-4" type="checkbox" checked={props.completed} onChange={toggleCompleted} />
                <button className="flex items-center" onClick={() => removeTodo(props.id)}>
                    <span className="material-symbols-rounded text-red-600">delete</span>
                </button>
            </div>
        </div>
    );
}