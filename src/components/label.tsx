interface ILabelProps {
    text: string
}

export default function Label(props: ILabelProps) {
    return (
        <label className="text-left font-serif mb-1">{props.text}</label>
    )
}