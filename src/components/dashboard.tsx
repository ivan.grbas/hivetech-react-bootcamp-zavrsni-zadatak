import { AddTodoModal } from "./addTodoModal";
import { useState } from 'react';
import TodoList from "./todoList";
import { UseAuthContext } from "../context/authContext";

export default function Dashboard() {
    const [isAddTodoOpen, setIsAddTodoOpen] = useState(false);
    const { logOut } = UseAuthContext();

    return (
        <div className="relative flex items-center flex-col bg-stone-200 shadow-xl w-full h-full max-w-7xl max-h-160 pb-12">
            <AddTodoModal isOpen={isAddTodoOpen} setIsOpen={setIsAddTodoOpen}/>
            <span className="text-2xl my-4 font-serif">My todos</span>
            <button className="absolute top-3 right-4 p-1" onClick={logOut}>
                <span className="material-symbols-outlined">mode_off_on</span>
            </button>
            <TodoList />
            <button onClick={() => setIsAddTodoOpen(true)} className="absolute -bottom-8 bg-green-400 rounded-full h-16 w-16 flex items-center justify-center">
                <span className="material-symbols-rounded text-white text-4xl">add</span>
            </button>
        </div>
    );
}