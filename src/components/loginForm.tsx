import { useForm } from "react-hook-form";
import { UseAuthContext } from "../context/authContext";
import "../styles/custom.css"
import Label from "./label";

export default function LoginForm() {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { logIn } = UseAuthContext();

    return (
        <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 border border-gray-500 w-full max-w-3xl rounded-md flex flex-col items-center">
            <span className="text-center mb-4 mt-4 font-serif text-xl">Log in</span>
            <form onSubmit={handleSubmit(logIn)} className="flex flex-col w-full items-center">
                <div className="w-11/12 mb-8">
                    <Label text="Email" />
                    <input {...register("email", { required: true, pattern: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ })} className="input"/>
                    <p className="text-left text-sm text-red-500 w-11/12">
                        {errors.email && "The email address is not in a valid format."}
                    </p>
                </div>
                <div className="w-11/12 mb-8">
                    <Label text="Password" />
                    <input type="password" {...register("password", { required: true, minLength: 6, pattern: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).+$/ })} className="input"/>
                    <p className="text-left text-sm text-red-500 w-11/12">
                        {errors.password && "Password must have at least 6 characters and contain 1 number, 1 lowercase and 1 upeercase letter."}
                    </p>
                </div>
                <input type="submit" className="mb-4 rounded-md py-1 bg-green-400 text-white w-1/6 hover:cursor-pointer" value="Log in"/>
            </form>
        </div>
    )
}