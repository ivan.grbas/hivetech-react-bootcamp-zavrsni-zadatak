import { UseTodoContext } from "../context/todoContext";
import Todo from "./todo";

export default function TodoList() {
    const { todos } = UseTodoContext();

    return (
        <div className="overflow-auto w-11/12 flex flex-col items-center">
            {todos.map((todo) => {
                return <Todo key={todo.id} {...todo}/>
            })}
        </div>
    )
}